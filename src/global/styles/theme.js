
export default {
  colors: {
    shape: '#fff',

    input_background: 'rgba(217, 217, 217, .4)',
    card_background: '#D9D9D9',
    background: '#fafafa',
    separator: '#A8A8A8',

    text_dark: '#000',
    text: '#FAFAFA',
  },

  fonts: {
    regular: 'Poppins_400Regular',
    medium: 'Poppins_500Medium',
    bold: ' Poppins_700Bold',
  },
};
