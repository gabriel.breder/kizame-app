import axios from 'axios';

const api = axios.create({
  baseURL: 'https://kizame.herokuapp.com/',
});

export default api;