import styled from 'styled-components/native';

export const BackgroundContainer = styled.View``;

export const Container = styled.View`
  flex: 1;
  align-items: center;
`;

export const InputWrapper = styled.View`
  margin-top: 40px;
  padding: 0 50px;
  align-items: center;
  width: 100%
`;

export const LogoContainer = styled.View`
  margin-top: 50px
`;
