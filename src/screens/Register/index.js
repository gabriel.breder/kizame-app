import React from 'react';

import { BackgroundContainer, Container, InputWrapper, LogoContainer } from './styles';
import { Input } from '../../components/Forms/Input'
import Background from '../../components/Background';
import { CategorySelect } from '../../components/Forms/CategorySelect';
import { Image } from 'react-native';

import Logo from '../../assets/images/logo.png'

const Register = () => {
  return (
    <BackgroundContainer>
      <Background>
        <Container>
          <LogoContainer>
            <Image source={Logo} />
          </LogoContainer>
          <InputWrapper>
            <Input placeholder={'Nome'} />
            <Input placeholder={'Sobrenome'} />
            <CategorySelect title='Modalidade' />
          </InputWrapper>
          <InputWrapper>
            <Input placeholder={'Email'} />
            <Input placeholder={'Senha'} />
            <Input placeholder={'Confirmar Senha'} />
          </InputWrapper>
        </Container>
      </Background>
    </BackgroundContainer>
  );
}

export default Register;
