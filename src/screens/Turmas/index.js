import React from 'react';
import Card from '../../components/Card';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import SearchInput from '../../components/SearchInput';

import { Container } from './styles';

const Turmas = () => {
  return (
    <Container>
      <Header title="TURMAS" />
      <SearchInput />
      <Card
        title="Karate"
      />
      <Footer
        icon="plus-circle"
      />
    </Container>
  );
}

export default Turmas;