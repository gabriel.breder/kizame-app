import React, { useContext, useState } from 'react';

import {
  BackgroundContainer,
  Container,
  InputWrapper,
  LogoContainer,
  LoginBtnContainer,
  RegisterBtnContainer,

} from './styles';

import { Input } from '../../components/Forms/Input'
import Background from '../../components/Background';
import { Image } from 'react-native';

import Logo from '../../assets/images/logo.png'
import Button from '../../components/Forms/Button';
import { GlobalContext } from '../../context/GlobalContext';

const Login = ({ navigation }) => {

  const context = useContext(GlobalContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  function handleLoginForm() {
    const body = {
      user: {
        email: 'professor@gmail.com',
        password: '123456'
      }
    }

    context.handleLogin(body, navigation)
  }

  return (
    <BackgroundContainer>
      <Background>
        <Container>
          <LogoContainer>
            <Image source={Logo} />
          </LogoContainer>

          <InputWrapper>
            <Input
              placeholder={'E-mail'}
              onChangeText={setEmail}
            />
            <Input
              placeholder={'Senha'}
              onChangeText={setPassword}
              secureTextEntry={true}
            />
          </InputWrapper>
          <LoginBtnContainer>
            <Button
              title="Login"
              onPress={handleLoginForm}
            />
          </LoginBtnContainer>
          <RegisterBtnContainer>
            <Button
              title="Inscreva-se"
              onPress={() => { navigation.navigate('Register') }}
            />
          </RegisterBtnContainer>

        </Container>
      </Background>
    </BackgroundContainer >
  );
}

export default Login;
