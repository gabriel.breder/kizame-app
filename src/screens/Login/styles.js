import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import styled from 'styled-components/native';

export const BackgroundContainer = styled.View``;

export const Container = styled.View`
  flex: 1;
  align-items: center;
  margin-top: ${getStatusBarHeight() + 52}px;
  padding: 0 35px;
`;

export const LogoContainer = styled.View`
  margin-bottom: 100px;
`;

export const InputWrapper = styled.View`
  width: 100%;
`;

export const LoginBtnContainer = styled.View`
  width: 70%;

  margin-top: 50px;
`;

export const RegisterBtnContainer = styled.View`
  width: 50%;
  margin-top: 120px;
`;


