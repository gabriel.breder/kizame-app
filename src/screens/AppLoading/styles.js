import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize'

export const Container = styled.View`
  flex: 1;
`;

export const LogoWrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.Text`
  color: ${({ theme }) => theme.colors.shape};
  font-size: ${RFValue(36)}px;
  font-family: ${({ theme }) => theme.fonts.regular};
`;

export const TitleStronger = styled.Text`
  color: ${({ theme }) => theme.colors.shape};
  font-size: ${RFValue(36)}px;
`;