import React from 'react';
import Background from '../../components/Background';

import {
  Container,
  LogoWrapper,
  Title,
  TitleStronger
} from './styles';

import { Image } from 'react-native'
import Logo from '../../assets/images/logo.png'

const AppLoading = () => {
  return (
    <Container>
      <Background>
        <LogoWrapper>
          <Image source={Logo} />
          <Title>Grow</Title>
          <TitleStronger>STRONGER</TitleStronger>
        </LogoWrapper>
      </Background>
    </Container>);
}

export default AppLoading;