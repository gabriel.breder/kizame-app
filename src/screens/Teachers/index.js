import React from 'react';

import { Container } from './styles';

import Header from '../../components/Header'
import SearchInput from '../../components/SearchInput';
import Card from '../../components/Card';

const Teachers = () => {
  return (
    <Container>
      <Header title="PROFESSORES" />
      <SearchInput />
      <Card />
    </Container>
  );
}

export default Teachers;