import React, { useContext } from 'react';
import { ScrollView, View } from "react-native";

import {
  Container,
  UploadSection
} from './styles';

import Header from '../../../components/Header';
import Profile from '../../../components/Profile';
import PersonalInfoSection from '../../../components/PersonalInfoSection';
import UploadButton from '../../../components/UploadButton';

import { GlobalContext } from '../../../context/GlobalContext'

const Student = () => {

  const context = useContext(GlobalContext)
  const user = context.user

  return (
    <Container>
      <ScrollView>
      <Header title="MEU PERFIL" />
      <Profile name={`${user.first_name} ${user.last_name}`} role={user.role} />
      <UploadSection>
        <UploadButton title="Treino" />
      </UploadSection>
      <PersonalInfoSection />
      </ScrollView>
    </Container >
  );
}

export default Student;