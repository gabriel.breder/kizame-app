export const categories = [
    { key: 'KAR', name: 'KARATE' },
    { key: 'JIU', name: 'JIU-JITSU' },
    { key: 'KRA', name: 'KRAV MAGA' },
    { key: 'ZUM', name: 'ZUMBA' },
    { key: 'MUA', name: 'MUAY THAI' },
    { key: 'BOX', name: 'BOXE' },
];

export const data = ["KARATE", "JIU-JITSU", "KRAV MAGA", "ZUMBA", "MUAY THAI", "BOXE"]