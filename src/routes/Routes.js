import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Login from '../screens/Login';
import Register from '../screens/Register';
import Administrator from '../screens/Profile/Administrator';
import Student from '../screens/Profile/Student';
import Teacher from '../screens/Profile/Teacher';
import Teachers from '../screens/Teachers';
import Turmas from '../screens/Turmas';

const Stack = createNativeStackNavigator();

export const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        <Stack.Screen
          name="Login"
          component={Login}
        />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Student" component={Student} />
        <Stack.Screen name="Teacher" component={Teacher} />
        <Stack.Screen name="Administrator" component={Administrator} />
        <Stack.Screen name="Teachers" component={Teachers} />
        <Stack.Screen name="Turmas" component={Turmas} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

