import styled from 'styled-components/native';

import { RFValue } from 'react-native-responsive-fontsize';
import Feather from '@expo/vector-icons/Feather'
import { TouchableOpacity } from 'react-native';


export const Container = styled(TouchableOpacity)`
  background-color: ${({ theme }) => theme.colors.text_dark};
  flex-direction: row;
  align-items: center;
  ${({ icon }) => icon ? `justify-content: space-between;` : `justify-content: center;`}
  
  padding: 10px 20px;
  border-radius: 5px;
  margin: 5px 0;
`;

export const Title = styled.Text`
  font-size: ${RFValue(20)}px;
  color: ${({ theme }) => theme.colors.shape};
`;

export const Icon = styled(Feather)`
  font-size: ${RFValue(30)}px;
  color: ${({ theme }) => theme.colors.shape};
`;
