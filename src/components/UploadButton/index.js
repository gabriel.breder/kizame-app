import React from 'react';

import { Container, Title, Icon } from './styles';

const UploadButton = ({ title, icon, onPress }) => {
  return (
    <Container icon={icon} onPress={onPress}>
      <Title>{title}</Title>
      {icon && <Icon name={icon} />}
    </Container>
  );
}

export default UploadButton;