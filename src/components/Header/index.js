import React from 'react';
import { Image } from 'react-native';
import { Entypo } from '@expo/vector-icons';

import { Container, Section } from './styles';

import LogoBlack from '../../assets/images/logo_black.png'
import User from '../../assets/images/user.png'
import Subtitle from '../Subtitle';

const Header = ({ title }) => {
  return (
    <Container>
      <Section>
        <Entypo name="menu" size={50} color="black" />
        <Image source={LogoBlack} />
        <Image source={User} />
      </Section>
      <Subtitle title={title} />
    </Container>
  );
}

export default Header;