import styled from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';

export const Container = styled.View`
  margin-top: ${getStatusBarHeight()}px;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const Section = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px;

`;
