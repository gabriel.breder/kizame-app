import React from 'react';
import { Separator } from '../Subtitle/styles';

import { Container, IconClick, Icon } from './styles';

const Footer = ({ icon, onPress }) => {
  return (
    <Container onPress={onPress}>
      <Separator />
      <IconClick>
        <Icon name={icon} />
      </IconClick>
    </Container>
  );
}

export default Footer;