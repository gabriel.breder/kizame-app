import styled from 'styled-components/native';

import Feather from '@expo/vector-icons/Feather'
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
`;

export const IconClick = styled.TouchableOpacity`
`;

export const Icon = styled(Feather)`
  font-size: ${RFValue(40)}px;
  text-align: center;
  margin: 10px 0;
`;