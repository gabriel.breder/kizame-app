import styled from 'styled-components/native';
import { TextInput } from 'react-native';

export const Container = styled.View`
  padding: 20px;
`;

export const Label = styled.Text`
  color: ${({ theme }) => theme.colors.card_background};
`;

export const Input = styled(TextInput).attrs((props) => ({
  placeholderTextColor: "#fff",
}))`
  background-color: ${({ theme }) => theme.colors.input_background};
  padding: 5px;
`;