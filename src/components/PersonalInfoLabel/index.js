import React from 'react';

import { Container, Label, Input } from './styles';

const PersonalInfoLabel = ({ text }) => {
  return (
    <Container>
      <Label>{text}</Label>
      <Input />
    </Container>
  );
}

export default PersonalInfoLabel;