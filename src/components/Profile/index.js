import React from 'react';
import { Image } from 'react-native';

import {
  Container,
  ProfileWrapper,
  ImageContainer,
  UserInfoContainer,
  Name,
  Role,
  Separator
} from './styles';

import User from '../../assets/images/user.png'

const Profile = ({ name, role }) => {
  return (
    <Container>
      <ProfileWrapper>
        <ImageContainer>
          <Image source={User} style={{ width: '100%', height: '100%' }} />
        </ImageContainer>
        <UserInfoContainer>
          <Name>{name}</Name>
          <Role>{role}</Role>
        </UserInfoContainer>
      </ProfileWrapper>
      <Separator />
    </Container>
  );
}

export default Profile;