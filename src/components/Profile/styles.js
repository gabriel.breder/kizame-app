import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';

export const Container = styled.View``;

export const ImageContainer = styled.View`
  width: 150px;
  height: 150px;
  margin: 20px auto;
`;

export const ProfileWrapper = styled.View`
  align-items: center;
`;

export const UserInfoContainer = styled.View`
  align-items: center;
`;

export const Name = styled.Text`
  font-size: ${RFValue(15)}px;
`;

export const Role = styled.Text`
  font-size: ${RFValue(15)}px;
  margin-bottom: 5px;
`;

export const Separator = styled.View`
  background-color: ${({ theme }) => theme.colors.separator};
  width: 100%;
  height: 1px;
`;