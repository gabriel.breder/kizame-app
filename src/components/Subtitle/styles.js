import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';

export const Container = styled.View`
  margin-top: 20px;
`;

export const Title = styled.Text`
  text-align: center;
  margin-bottom: 5px;
  font-size: ${RFValue(15)}px;
`;

export const Separator = styled.View`
  background-color: ${({ theme }) => theme.colors.separator};
  height: 1px;
  width: 100%;
`;