import React from 'react';

import { Container, Title, Separator } from './styles';

const Subtitle = ({ title }) => {
  return (
    <Container>
      <Title>{title}</Title>
      <Separator />
    </Container>
  );
}

export default Subtitle;