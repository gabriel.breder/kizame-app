import React from 'react';
import { ImageBackground } from 'react-native';

// import { Container } from './styles';

const Background = ({ children }) => {
  return (
    <ImageBackground
      source={require('../../assets/images/background_img.jpg')}
      style={{ width: '100%', height: '100%' }}
    >
      {children}
    </ImageBackground>
  );
}

export default Background;