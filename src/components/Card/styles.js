import styled from 'styled-components/native';
import Feather from '@expo/vector-icons/Feather'
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 10px;
  margin: 10px;
  border-radius: 5px;

  background-color: ${({ theme }) => theme.colors.card_background};
`;

export const UserContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const Name = styled.Text`
  margin-left: 10px;
  font-size: ${RFValue(18)}px;
`;

export const Icon = styled(Feather)`
  font-size: ${RFValue(20)}px;
`;