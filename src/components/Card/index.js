import React from 'react';

import { Container, UserContainer, Name, Icon } from './styles';
import User from '../../assets/images/user.png'
import { Image } from 'react-native';

const Card = ({ image, title }) => {
  return (
    <Container>
      <UserContainer>
        {image && <Image source={image} />}
        <Name>{title}</Name>
      </UserContainer>
      <Icon name="trash-2" />
    </Container>
  );
}

export default Card;