import React from 'react';
import PersonalInfoLabel from '../PersonalInfoLabel';
import Subtitle from '../Subtitle';

import { Container } from './styles';

const PersonalInfoSection = () => {
  return (
    <Container>
      <Subtitle title="INFORMAÇÕES PESSOAIS" />
      <PersonalInfoLabel text="NOME" />
      <PersonalInfoLabel text="E-MAIL" />
      <PersonalInfoLabel text="SENHA" />
    </Container>
  );
}

export default PersonalInfoSection;