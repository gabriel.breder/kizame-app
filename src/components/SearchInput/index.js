import React, { useState } from 'react';

import { Container } from './styles';

import { SearchBar } from 'react-native-elements'

import theme from '../../global/styles/theme'

const SearchInput = () => {

  const [search, setSearch] = useState('');

  return (
    <Container>
      <SearchBar
        placeholder='Pesquisar'
        onChangeText={setSearch}
        value={search}
        lightTheme={false}
        containerStyle={{
          backgroundColor: theme.colors.background,
          borderBottomColor: 'transparent',
          borderTopColor: 'transparent',
        }}
      />
    </Container>
  );
}

export default SearchInput;