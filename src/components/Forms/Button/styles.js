import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.background};
  width: 100%;
  padding: 10px 16px;
  border-radius: 5px;
`;

export const Title = styled.Text`
  text-align: center;
  font-size: ${RFValue(15)}px;
  font-weight: bold;
`;
