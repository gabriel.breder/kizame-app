import React from 'react';
import { ButtonProps } from 'react-native';
import { Container, Title } from './styles';


const Button = ({ onPress, title, onMouseDown }) => {
  return (
    <Container onPress={onPress} onMouseDown={onMouseDown}>
      <Title>{title}</Title>
    </Container>
  );
}

export default Button;