import styled from 'styled-components/native';
import { TextInput } from 'react-native';

export const Container = styled(TextInput).attrs((props) => ({
  placeholderTextColor: "#fff",
}))`
  width: 100%;
  padding: 10px 16px;
  text-align: center;

  color: #fff;
  background-color: ${({ theme }) => theme.colors.input_background};
  border-radius: 5px;

  margin-bottom: 20px;

  ::placeholder,
  ::-webkit-input-placeholder {
    color: #fafafa;
  }
  :-ms-input-placeholder {
     color: #fafafa;
  }
`;
