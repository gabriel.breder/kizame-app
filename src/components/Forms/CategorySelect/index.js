import React from 'react';
import { RFValue } from 'react-native-responsive-fontsize';
import SelectDropdown from 'react-native-select-dropdown'

import { data } from '../../../utils/modalidades';

import {
    Container,
    Category,
    Icon
} from './styles';

import theme from '../../../global/styles/theme'

export function CategorySelect({ title }) {

    return (
        <Container>
            <SelectDropdown
                data={data}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonStyle={{
                    backgroundColor: 'transparent',
                    width: '100%',
                    margin: 0,
                    
                    
                }}
                defaultButtonText={title}
                buttonTextStyle={{
                    color: '#fff',
                    fontSize: RFValue(13),
                    marginLeft: 30,
                    
                }}
                renderDropdownIcon={() =>
                    <Icon name='chevron-down' color='white' size={22} />
                }
                dropdownStyle={{
                    backgroundColor: theme.colors.shape,
                    borderRadius: 5,
                    margin: 0,
                    marginTop: -20
                }}
                dropdownOverlayColor='transparent'
            >
            </SelectDropdown>
        </Container >
    )

}