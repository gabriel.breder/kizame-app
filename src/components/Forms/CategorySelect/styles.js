import styled from 'styled-components/native';
import { TextInput } from 'react-native';
import { Feather } from '@expo/vector-icons'

export const Container = styled.TouchableOpacity.attrs({
    activeOpacity: 0.7
})`
    width: 100%;
    align-items: center;
    color: #FAFAFA;
    background-color: ${({ theme }) => theme.colors.input_background};
    border-radius: 5px;
    margin-bottom: 8px;
    flex-direction: row;
  `;
export const Icon = styled(Feather)`
`;
