import { useState } from "react";
import api from "../../services/api";

export function useAuth() {

  const [user, setUser] = useState('')
  const [error, setError] = useState(false)
  const [errorMessage, setErrorMessage] = useState(false)

  const handleLogin = (body, navigation) => {
    api.post('/login', body)
      .then(res => {
        setUser(res.data.user)
        console.log(res.data.user)

        const role = res.data.user.role

        if (role === "student") {
          navigation.navigate("Student")
        } else if (role === "teacher") {
          navigation.navigate("Teacher")
        } else if (role === "admin") {
          navigation.navigate("Administrator")
        }
      })
      .catch((error) => {
        console.log(error)
        setError(true)
        setErrorMessage(true)
      })

  }


  return { handleLogin, user, error, setError, errorMessage }
}
