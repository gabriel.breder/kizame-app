import { createContext } from "react";
import { useAuth } from "./Hooks/useAuth";

const GlobalContext = createContext()

function GlobalProvider({ children }) {
  const {
    handleLogin,
    user,
    error,
    setError,
    errorMessage
  } = useAuth()

  return (
    <GlobalContext.Provider
      value={
        {
          handleLogin,
          user,
          error,
          setError,
          errorMessage
        }
      }>
      {children}
    </GlobalContext.Provider>
  )
}

export { GlobalContext, GlobalProvider }
