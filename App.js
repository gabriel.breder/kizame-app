import { ThemeProvider } from 'styled-components';
import theme from './src/global/styles/theme';

import AppLoading from './src/screens/AppLoading';
import Login from './src/screens/Login';
import Register from './src/screens/Register';
import Administrator from './src/screens/Profile/Administrator';
import { NavigationContainer } from '@react-navigation/native';
import { Routes } from './src/routes/Routes';
import { GlobalProvider } from './src/context/GlobalContext';


export default function App() {
  return (

    <GlobalProvider>
      <ThemeProvider theme={theme}>
        <Routes />
      </ThemeProvider>
    </GlobalProvider>
  );
}